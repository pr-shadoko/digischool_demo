<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class UserController extends AbstractController {
    /**
     * Retrieves the user votes.
     *
     * HTTP method: GET
     * Parameters:
     *  - int user User identifier
     *
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function index(User $user) {
        if(is_null($user)) {
            return $this->json([
                'success' => false,
                'message' => 'The requested user does not exist.',
            ],
            404);
        }

        //TODO: No simple way to convert entities to arrays? Seriously?
        $votes = [];
        foreach($user->getVotes() as $vote) {
            $votes[] = $vote->getMovie()->toArray();
        }

        return $this->json([
            'success' => true,
            'message' => 'Votes for user ' . $user->getPseudo() . ' were successfully retrieved.',
            'data' => $votes,
        ]);
    }

    /**
     * Creates a new user.
     *
     * HTTP Method: POST
     * Parameters:
     *  - string pseudo
     *  - string email
     *  - string birth_date Date formatted as YYYY-MM-DD
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function create(Request $request) {
        $pseudo = $request->request->get('pseudo');
        $email = $request->request->get('email');
        $birthDate = $request->request->get('birth_date');

        if(is_null($pseudo ?? $email ?? $birthDate ?? null)) {
            //TODO: alternative error format : https://tools.ietf.org/html/draft-nottingham-http-problem-07
            return $this->json([
                'success' => false,
                'message' => 'A parameter is missing. Expected parameters are "pseudo", "email" and "birthdate".',
            ],
            //TODO: use a more meaningful error code
            400);
        }

        //TODO: Validate input

        $user = new User();
        $user->setPseudo($pseudo);
        $user->setEmail($email);
        $user->setBirthDate(new \DateTime($birthDate));

        try {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
        } catch(UniqueConstraintViolationException $e) {
            return $this->json([
                'success' => false,
                'message' => 'Given pseudo/email already exists.',
            ],
            //TODO: use a more meaningful error code
            400);
        }

        return $this->json([
            'success' => true,
            'message' => 'User ' . $pseudo . ' was successfully created.',
            'data' => $user->toArray(),
        ],
        201);
    }
}
