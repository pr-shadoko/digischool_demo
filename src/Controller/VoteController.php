<?php

namespace App\Controller;

use App\Entity\Movie;
use App\Entity\User;
use App\Entity\Vote;
use App\Omdb\OmdbRequest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class VoteController extends AbstractController {
    /**
     * Creates a new vote.
     *
     * HTTP method: POST
     * Parameters:
     *  - int user_id User identifier
     *  - string movie_id Movie IMDB identifier
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function create(Request $request) {
        //TODO: use a token to identify user instead of its id
        $userId = $request->request->get('user_id');
        $movieId = $request->request->get('movie_id');

        if(is_null($userId ?? $movieId ?? null)) {
            return $this->json([
                'success' => false,
                'message' => 'A parameter is missing. Expected parameters are "user_id" and "movie_id".',
            ],
            //TODO: use a more meaningful error code
            400);
        }

        $em = $this->getDoctrine()->getManager();
        $userRepo = $em->getRepository(User::class);
        $user = $userRepo->find($userId);

        if(is_null($user)) {
            return $this->json([
                'success' => false,
                'message' => 'The user does not exist.',
            ],
            404);
        }

        $movieRepo = $em->getRepository(Movie::class);
        $movie = $movieRepo->find($movieId);

        $voteRepo = $em->getRepository(Vote::class);
        $vote = $voteRepo->findOneBy(['user' => $user, 'movie' => $movie]);
        if(!is_null($vote)) {
            return $this->json([
                'success' => false,
                'message' => 'User ' . $user->getPseudo() . ' has already voted for ' . $movie->getTitle() . '.',
            ],
            //TODO: use a more meaningful error code
            400);
        }

        $initialVotesCount = $voteRepo->countUserVotes($user);
        if($initialVotesCount >= Vote::MAX_USER_VOTES_COUNT) {
            return $this->json([
                'success' => false,
                'message' => 'Maximum votes count has already been reached for user ' . $user->getPseudo() . '.',
            ],
            //TODO: use a more meaningful error code
            400);
        }

        if(is_null($movie)) {
            $response = (new OmdbRequest($movieId))->execute();
            if($response === false) {
                return $this->json([
                    'success' => false,
                    'message' => 'OMDB request could not be completed.',
                ],
                //TODO: use a more meaningful error code
                400);
            }
            if($response->Response === "False") {
                return $this->json([
                    'success' => false,
                    'message' => $response->Error,
                ],
                // TODO: use a more meaningful error code
                400);
            }

            $movie = (new Movie())
                ->setId($movieId)
                ->setTitle($response->Title)
                ->setPoster($response->Poster);
            $em->persist($movie);
        }

        $vote = (new Vote())
            ->setUser($user)
            ->setMovie($movie);
        $em->persist($vote);
        $em->flush();

        return $this->json([
            'success' => true,
            'message' => 'Vote for movie ' . $movie->getTitle() . ' by ' . $user->getPseudo() . ' was successfully created.',
            'data' => [
                'votes_count' => $initialVotesCount + 1,
            ],
        ],
        201);
    }

    /**
     * Deletes a vote.
     *
     * HTTP method: DELETE
     * Parameters:
     *  - int user_id User identifier
     *  - string movie_id Movie IMDB identifier
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function delete(Request $request) {
        //TODO: use a token to identify user instead of its id
        $userId = $request->query->get('user_id');
        $movieId = $request->query->get('movie_id');

        if(is_null($userId ?? $movieId ?? null)) {
            return $this->json([
                'success' => false,
                'message' => 'A parameter is missing. Expected parameters are "user_id" and "movie_id".',
            ],
            //TODO: use a more meaningful error code
            400);
        }

        $em = $this->getDoctrine()->getManager();
        $voteRepo = $em->getRepository(Vote::class);
        $vote = $voteRepo->findOneBy(['user' => $userId, 'movie' => $movieId]);
        if(is_null($vote)) {
            return $this->json([
                'success' => false,
                'message' => 'The vote to delete does not exist.',
            ],
            404);
        }

        $em->remove($vote);
        $em->flush();

        return $this->json([
            'success' => true,
            'message' => 'The vote has been deleted.',
        ]);
    }

    /**
     * Retrieves the most voted movies.
     *
     * HTTP method: GET
     * Parameters: none
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function best() {
        $voteRepo = $this->getDoctrine()->getRepository(Vote::class);
        $mostVotedMovies = $voteRepo->getMostVotedMovies();

        $movies = [];
        foreach($mostVotedMovies as $movie) {
            $movies[] = $movie->toArray();
        }

        return $this->json([
            'success' => true,
            'message' => 'Most voted movies were successfully retrieved.',
            'data' => $movies,
        ]);
    }
}
