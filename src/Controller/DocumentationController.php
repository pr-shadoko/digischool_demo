<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DocumentationController extends Controller {
    public function index() {
        return $this->json([
            'api_doc' => [
                'version' => 1,
                'endpoints' => [
                    [
                        'route' => '/user/create',
                        'method' => 'POST',
                        'params' => [
                            [
                                'key' => 'pseudo',
                                'constraints' => 'String.',
                            ],
                            [
                                'key' => 'email',
                                'constraints' => 'String. A valid email address.',
                            ],
                            [
                                'key' => 'birth_date',
                                'constraints' => 'String. Accepted format is YYYY-MM-DD.',
                            ],
                        ],
                        'action' => 'Creates a new user with the given data. Returns the user instance.',
                    ],
                    [
                        'route' => '/user/votes/{user}',
                        'method' => 'GET',
                        'params' => [
                            [
                                'key' => 'user',
                                'constraints' => 'Integer. An existing user identifier.',
                            ],
                        ],
                        'action' => 'Returns the list of movies the user voted for.',
                    ],
                    [
                        'route' => '/vote/create',
                        'method' => 'POST',
                        'params' => [
                            [
                                'key' => 'user_id',
                                'constraints' => 'Integer. An existing user identifier.',
                            ],
                            [
                                'key' => 'movie_id',
                                'constraints' => 'String. A valid IMDB movie identifier.',
                            ],
                        ],
                        'action' => 'Saves a user\'s vote for a movie.',
                    ],
                    [
                        'route' => '/vote/delete',
                        'method' => 'DELETE',
                        'params' => [
                            [
                                'key' => 'user_id',
                                'constraints' => 'Integer. An existing user identifier.',
                            ],
                            [
                                'key' => 'movie_id',
                                'constraints' => 'String. A valid IMDB movie identifier.',
                            ],
                        ],
                        'action' => 'Removes a user\'s vote for a movie.',
                    ],
                    [
                        'route' => '/vote/best',
                        'method' => 'GET',
                        'params' => [],
                        'action' => 'Returns the list of movies having the most votes.',
                    ],
                    [
                        'route' => '/movie/votes/{movie}',
                        'method' => 'GET',
                        'params' => [
                            [
                                'key' => 'movie',
                                'constraints' => 'String. A valid IMDB movie identifier.',
                            ],
                        ],
                        'action' => 'Returns the list of users who voted for a movie.',
                    ],
                ],
            ],
        ]);
    }
}
