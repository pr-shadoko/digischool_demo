<?php

namespace App\Controller;

use App\Entity\Movie;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MovieController extends AbstractController {
    /**
     * Retrieves the movie voters.
     *
     * HTTP method: GET
     * Parameters:
     *  - int movie Movie identifier
     *
     * @param Movie $movie
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function index(Movie $movie) {
        if(is_null($movie)) {
            return $this->json([
                'success' => false,
                'message' => 'The requested movie does not exist.',
            ],
            404);
        }

        //TODO: No simple way to convert entities to arrays? Seriously?
        $voters = [];
        foreach($movie->getVotes() as $vote) {
            $voters[] = $vote->getUser()->toArray();
        }

        return $this->json([
            'success' => true,
            'message' => 'Voters for movie ' . $movie->getTitle() . ' were successfully retrieved.',
            'data' => $voters,
        ]);
    }
}
