<?php

namespace App\Omdb;

class OmdbRequest {
    private $URL = 'http://www.omdbapi.com/';
    private $apiKey;
    private $movieId;

    /**
     * OmdbRequest constructor.
     * @param string $movieId
     */
    public function __construct(string $movieId) {
        $this->apiKey = getenv('OMDB_API_KEY');
        $this->movieId = $movieId;
    }

    public function execute() {
        $curlHandle = curl_init($query = $this->URL . '?apikey=' . $this->apiKey . '&i=' . $this->movieId);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curlHandle);
        curl_close($curlHandle);
        return $response === false ? false : json_decode($response);
    }
}
