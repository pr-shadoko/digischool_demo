<?php

namespace App\Listener;

use App\Entity\Timestampable;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Psr\Log\LoggerInterface;

class TimestampableListener {
    private $logger;

    public function __construct(LoggerInterface $logger) {
        $this->logger = $logger;
    }

    public function prePersist(LifecycleEventArgs $eventArgs) {
        $object = $eventArgs->getEntity();

        if($object instanceof Timestampable) {
            $datetime = new \DateTime();
            $object->setCreatedAt($datetime);
            $object->setUpdatedAt($datetime);
        }
    }

    public function preUpdate(PreUpdateEventArgs $eventArgs) {
        $object = $eventArgs->getEntity();

        if($object instanceof Timestampable) {
            $object->setUpdatedAt(new \DateTime());
        }
    }
}