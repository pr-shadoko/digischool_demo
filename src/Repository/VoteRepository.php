<?php

namespace App\Repository;

use App\Entity\Movie;
use App\Entity\User;
use App\Entity\Vote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Vote|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vote|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vote[]    findAll()
 * @method Vote[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VoteRepository extends ServiceEntityRepository {
    public function __construct(RegistryInterface $registry) {
        parent::__construct($registry, Vote::class);
    }

    /**
     * @param User $user
     * @return int The user votes count
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countUserVotes(User $user) {
        return $this->createQueryBuilder('v')
            ->select('COUNT(v.user)')
            ->where('v.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();
        // select count(*) from `vote` where user_id = :user_id;
    }

    /**
     * @return array The most voted movies list
     */
    public function getMostVotedMovies() {
        // Due to some MySQL mystery this does not work :
        // select `id`, `title`, `poster`, max(`vote_count`)
        // from `movie` `m`
        // join (
        //   select `movie_id`, count(*) as `vote_count` from `vote` group by `movie_id`
        // ) `v` on `m`.`id` = `v`.`movie_id`;
        // It returns the right max, but the other columns are filled with the first row of the movie table…
        // See https://stackoverflow.com/questions/18051729/mysql-select-max-from-sub-query-with-count
        //
        // So we have to use this :
        $query = 'select `m`.`id`, `m`.`title`, `m`.`poster`
        from `movie` `m`
        join (
          select `movie_id`, count(*) as `vote_count`
          from `vote`
          group by `movie_id`
        ) `v` on `m`.`id` = `v`.`movie_id`
        where `v`.`vote_count` = (
          select max(`c`.`count`) from (
            select count(*) as `count`
            from `vote`
            group by `movie_id`
          ) `c`
        );';

        $rsm = new ResultSetMappingBuilder($this->getEntityManager());
        $rsm->addRootEntityFromClassMetadata(Movie::class, 'm');

        return $this->getEntityManager()->createNativeQuery($query, $rsm)
            ->getResult();
    }
}
