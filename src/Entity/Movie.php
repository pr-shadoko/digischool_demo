<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MovieRepository")
 */
class Movie {
    /**
     * @ORM\Id()
     * @ORM\Column(type="string")
     * @var string
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $poster;

    /**
     * @ORM\OneToMany(targetEntity="Vote", mappedBy="movie")
     * @var Collection
     */
    private $votes;

    /**
     * Movie constructor.
     */
    public function __construct() {
        $votes = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getId(): string {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Movie
     */
    public function setId(string $id): Movie {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Movie
     */
    public function setTitle(string $title): Movie {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getPoster(): string {
        return $this->poster;
    }

    /**
     * @param string $poster
     * @return Movie
     */
    public function setPoster(string $poster): Movie {
        $this->poster = $poster;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getVotes(): Collection {
        return $this->votes;
    }

    /**
     * @param Collection $votes
     * @return Movie
     */
    public function setVotes(Collection $votes): Movie {
        $this->votes = $votes;
        return $this;
    }

    /**
     * Converts entity to array.
     *
     * TODO: Find a nicer way to do that. Seriously.
     * @return array
     */
    public function toArray() {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'poster' => $this->getPoster(),
        ];
    }
}
