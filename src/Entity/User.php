<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements Timestampable {
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20, unique=true)
     * @var string
     */
    private $pseudo;

    /**
     * @ORM\Column(type="string", length=254, unique=true)
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(type="date")
     * @var \DateTime
     */
    private $birthDate;

    /**
     * @ORM\OneToMany(targetEntity="Vote", mappedBy="user")
     * @var Collection
     */
    private $votes;

    /**
     * User constructor.
     */
    public function __construct() {
        $this->votes = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param int $id
     * @return User
     */
    public function setId(int $id): User {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getPseudo(): string {
        return $this->pseudo;
    }

    /**
     * @param string $pseudo
     * @return User
     */
    public function setPseudo(string $pseudo): User {
        $this->pseudo = $pseudo;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string {
        return $this->email;
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail(string $email): User {
        $this->email = $email;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBirthDate(): \DateTime {
        return $this->birthDate;
    }

    /**
     * @param \DateTime $birthDate
     * @return User
     */
    public function setBirthDate(\DateTime $birthDate): User {
        $this->birthDate = $birthDate;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getVotes(): Collection {
        return $this->votes;
    }

    /**
     * @param Collection $votes
     * @return User
     */
    public function setVotes(Collection $votes): User {
        $this->votes = $votes;
        return $this;
    }

    /**
     * Converts entity to array.
     *
     * TODO: Find a nicer way to do that. Seriously.
     * @return array
     */
    public function toArray() {
        return [
            'id' => $this->getId(),
            'pseudo' => $this->getPseudo(),
            'email' => $this->getEmail(),
            'birthDate' => $this->getBirthDate(),
        ];
    }
}
