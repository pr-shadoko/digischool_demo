<?php

namespace App\Entity;

interface Timestampable {
    public function getCreatedAt(): \DateTime;

    public function setCreatedAt(\DateTime $createdAt): Timestampable;

    public function getUpdatedAt(): \DateTime;

    public function setUpdatedAt(\DateTime $updatedAt): Timestampable;
}