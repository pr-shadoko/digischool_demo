<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VoteRepository")
 */
class Vote {
    const MAX_USER_VOTES_COUNT = 3;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="User", inversedBy="votes")
     * @var User
     */
    private $user;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Movie", inversedBy="votes")
     * @var Movie
     */
    private $movie;

    /**
     * @return User
     */
    public function getUser(): User {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Vote
     */
    public function setUser(User $user): Vote {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Movie
     */
    public function getMovie(): Movie {
        return $this->movie;
    }

    /**
     * @param Movie $movie
     * @return Vote
     */
    public function setMovie(Movie $movie): Vote {
        $this->movie = $movie;
        return $this;
    }
}
